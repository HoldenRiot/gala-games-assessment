/// <reference types="cypress" />

it('should be able to search for item: Best Friend from Store page', () => {
    cy.visit('https://app.gala.games/store', { timeout: 150000 }) // Navigate to Gala Games Store
    cy.xpath("//div[contains(@id,'usercentrics-root')]").click({ force: true }) // Click `Accept All` on Privacy Settings
    cy.xpath("//input[@placeholder='Search']").click({ force: true }).type('Best Friend') // Click Search text box and search for 'Best Friend' item.
    cy.xpath("//p[@class='font-weight-medium mb-0 fs-18']").should('contain.text', 'Best Friend') // Validate the search result is 'Best Friend' item as expected.
    cy.xpath("//p[@class='mb-0 text-right game-name medium-font']").should('contain.text', 'Gala Music') // Validate the search result is 'Best Friend' item as expected.
})
