/// <reference types="cypress" />

it('should not be able to launch Town Star without being logged in', () => {
    cy.visit('https://app.gala.games/games', { timeout: 150000 }) // Navigate to Gala games web page
    cy.xpath("//div[contains(@id,'usercentrics-root')]").click({ force: true }) // Click `Accept All` on Privacy Settings
    cy.xpath("//h2[contains(.,'Town Star')]").scrollIntoView({ duration: 1000 }) // Scroll to Town Star
    cy.xpath("//button[contains(.,'Play')]").click({ force: true }) // Click Play button
    cy.xpath("//h2[@class='mb-3 mt-10 text-center text--uppercase']").should('contain.text', 'Create your Account') // Validate 'Create your Account' text is displayed as expected.
    cy.xpath("//p[@class='text-center mt-6']").should('contain.text', 'Already a member?') // Validate 'Already a member' text is displayed as expected.
})
