/// <reference types="cypress" />

it('should be able to filter store page by game: Town Star with items with Epic Rarity', () => {
    cy.visit('https://app.gala.games/store', { timeout: 150000 }) // Navigate to Gala Games Store
    cy.xpath("//div[contains(@id,'usercentrics-root')]").click({ force: true }) // Click `Accept All` on Privacy Settings
    cy.xpath("//p[@class='mb-0 fs-18'][contains(.,'Town Star')]").click({ force: true }) // Click filter: `Town Star` under Games on Store page.
    cy.xpath("(//p[contains(.,'Town Star')])[2]").should('contain.text', 'Town Star')  // Validate Store page is being filtered by `Town Star` game.
    cy.xpath("(//i[contains(@aria-hidden,'true')])[10]").click({ force: true }) // Check Store Rarity filter: `Epic`.
    cy.xpath("(//p[contains(.,'Epic')])[2]").should('contain.text', 'Epic') // Validate Store page is being filtered by 'Epic' Rarity.
})
