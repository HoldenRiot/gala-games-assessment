# gala-games-assessment
A Cypress.io Gala Games Coding challenge assessment.

# Requirements
1. OS: macOS 10.9 and above (64-bit only), Linux Ubuntu 12.04 and above, Fedora 21 and Debian 8 (64-bit only), or Windows 7 and above (64-bit only).
2. [npm](https://nodejs.org/en/download/)
 - To check whether you already have Node installed, open a new *terminal* window and type: `node -v`. If you have Node.js installed, it should output Node’s version. If you don’t, you’ll see *command not found: node.*
 - To Install NPM, you can download and run the installed from the Node.js [website](https://nodejs.org/en/download/), or use [homebrew (MacOS only)](https://brew.sh/) by running: `brew update` , followed by `brew install node`.

# Test Scenarios
- From the Games page I should not be able to launch Town Star without being logged in. ([townstarlogin.spec.js](https://gitlab.com/HoldenRiot/gala-games-assessment/-/blob/master/cypress/integration/townstarlogin.spec.js))
- From the Store page Search for an item of your choice. ([search.spec.js](https://gitlab.com/HoldenRiot/gala-games-assessment/-/blob/master/cypress/integration/search.spec.js))
- From the Store page I should be able to filter Town Star items by Epic Rarity. ([townstarfilter.spec.js](https://gitlab.com/HoldenRiot/gala-games-assessment/-/blob/master/cypress/integration/townstarfilter.spec.js))
- From the Store page I should be able to filter Spider Tank items by Rare Rarity. ([spidertanksfilter.spec.js](https://gitlab.com/HoldenRiot/gala-games-assessment/-/blob/master/cypress/integration/spidertanksfilter.spec.js))

## Test scenario file location
 - cypress > integration

# Setup
1. Clone the [gala-games-assessment repository](https://gitlab.com/HoldenRiot/gala-games-assessment).
2. From the project directory, run: `npm install`   (This will install [Cypress.io](https://www.cypress.io/))

# Running tests
- To run headless: `npm test` 
- To run with browser: `npm run test:browser`
- Tests can also be executed individually using: `npm run test:open` and clicking on the file names.
