/// <reference types="cypress" />

it('should be able to filter store page by game: Spider Tanks items with Rare Rarity', () => {
    cy.visit('https://app.gala.games/store', { timeout: 150000 }) // Navigate to Gala Games Store
    cy.xpath("//div[contains(@id,'usercentrics-root')]").click({ force: true }) // Click `Accept All` on Privacy Settings
    cy.xpath("//p[@class='mb-0 fs-18'][contains(.,'Spider Tanks')]").click({ force: true }) // Click filter: `Spider Tanks` under Games on Store page.
    cy.xpath("(//p[contains(.,'Spider Tanks')])[2]").should('contain.text', 'Spider Tanks')  // Validate Store page is being filtered by `Spider Tanks` game.
    cy.xpath("(//i[contains(@aria-hidden,'true')])[9]").click({ force: true }) // Check Store Rarity filter: `Rare`.
    cy.xpath("(//p[contains(.,'Rare')])[2]").should('contain.text', 'Rare') // Validate Store page is being filtered by 'Rare' Rarity.
    cy.xpath("(//p[@class='mb-0 text-right game-name medium-font'][contains(.,'Spider Tanks')])[1]").should('contain.text', 'Spider Tanks') // Validate search results contain Spider Tanks items.
})
